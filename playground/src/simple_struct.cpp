//
// Created by slawtul on 02/03/24.
//


#include <iostream>
#include "./simple_struct.h"

int main() {
    simple_struct ss{};

    std::cout << ss.name << "\n";
    std::cout << ss.d_number << "\n";
    std::cout << ss.digit << "\n";
    std::cout << ss.truth << "\n";
    std::cout << ss.sign << "\n";
    return 0;
}
