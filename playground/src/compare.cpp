#include <cstddef>
#include <iostream>
#include <utility>

int main(int argc, char const *argv[]) {
    long a = -100;
    size_t b = 100;

    std::cout << "long a=-100\n";
    std::cout << "size_t b=100\n";
    if (a < b) std::cout << "a < b with if statement\n";
    if (a > b) std::cout << "a > b with if statement\n";

    if (std::cmp_less(a, b)) {
        std::cout << "a < b with std::cmp_less(x, y)\n";
    }

    return 0;
}
