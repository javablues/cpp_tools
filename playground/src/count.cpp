#include <iostream>
#include <vector>
#include <algorithm>
#include <ranges>

int main() {
    std::vector<std::string> books{"Hello world", "Hello programmer", "Salut computer", "Hello"};
    int res = std::count(books.begin(), books.end(), "Hello world");
    std::cout << "Found: " << res << " Hello world in the vector\n"

    int len = std::ranges::size("Hello world");
    std::cout << "Size: " << len << " signs\n";

    int v_sz = std::ranges::size(books);
    std::cout << "Vector size (with ranges): " << v_sz << "\n";
    std::cout << "Vector size (with books.size()): " << books.size() << "\n";

    return 0;
}

// g++ --version: 10.3.0
// g++ -std=c++20 -Wall -Wextra -Werror count.cpp -o count && ./count
