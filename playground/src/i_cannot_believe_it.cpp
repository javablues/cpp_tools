#include <iostream>

int main(int argc, char const *argv[]) {
    std::string str{"Modern C++"};

    std::string s1{"Modern C++", 3};
    std::string s2{str, 3};

    std::cout << "S1: " << s1 << "\n";
    std::cout << "S2: " << s2 << "\n";

    //output
    //S1: Mod
    //S2: ern C++

    return 0;
}

